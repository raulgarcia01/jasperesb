package sv.gob.mh.dgcg.safim.impl.queue;

import java.io.Serializable;
import java.sql.Connection;

import javax.sql.DataSource;

import org.mule.api.MuleContext;
import org.mule.api.MuleEventContext;
import org.mule.api.annotations.expressions.Lookup;
import org.mule.api.lifecycle.Callable;
import org.mule.transport.jdbc.JdbcConnector;

import sv.gob.mh.dgcg.safim.util.queue.ParamMQ;
import sv.gob.mh.dgcg.safim.util.queue.ReportMQ;

public class ReportImpl implements Serializable, Callable {

	/**
	 * Serial de la Clase
	 */
	private static final long serialVersionUID = 458417244987613199L;

	@Lookup
	private MuleContext muleContext;

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {

		ReportMQ reportData = (ReportMQ) eventContext.getMessage().getPayload();

		// --Extrayendo el Connector PostgreSql Configurado
		JdbcConnector jdbcConnector = (JdbcConnector) muleContext.getRegistry()
				.lookupConnector("PostgreSQL_DB");
		DataSource dataSource = (DataSource) jdbcConnector.getDataSource();
		Connection conn = dataSource.getConnection();

		if (!conn.isClosed()) {
			conn.close();
		}

		System.out.println(reportData.getReporte());
		for (ParamMQ row : reportData.getParametros()) {
			System.out.println(row.getNombre());
		}
		return eventContext.getMessage();
	}

	public MuleContext getMuleContext() {
		return muleContext;
	}

	public void setMuleContext(MuleContext muleContext) {
		this.muleContext = muleContext;
	}

}
