/* Copyright 2013 - 2014
 * Dirección General de Contabilidad Gubernamental
 * Ministerio de Hacienda
 *
 * Este programa es de uso exclusivo para la Unidad de Informática
 * de la Dirección General de Contabilidad Gubernamental,
 * y está protegido por las leyes de derechos de autor de la
 * República de El Salvador, se prohibe la copia y distribución
 * total o parcial del mismo.
 */
package sv.gob.mh.dgcg.safim.util.queue;

import java.io.Serializable;

/**
 * Clase de parámetros de los reportes.
 *
 * @author Luis Felipe Sosa Alvarez <luisfsosa@gmail.com>
 */
public class ParamMQ implements Serializable {

    /**
     * Serial de la Clase.
     */
    private static final long serialVersionUID = 7336246282744512633L;

    /**
     * Nombre del Parametro.
     */
    private String nombre;

    /**
     * Valor del Parametro.
     */
    private String valor;

    /**
     * Retorna el valor de Nombre del Parametro.
     *
     * @return Nombre del Parametro
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Modifica el valor de Nombre del Parametro.
     *
     * @param Nombre
     *            del Parametro
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Retorna el valor de Valor del Parametro.
     *
     * @return Valor del Parametro
     */
    public String getValor() {
        return valor;
    }

    /**
     * Modifica el valor de Valor del Parametro.
     *
     * @param Valor
     *            del Parametro
     */
    public void setValor(String valor) {
        this.valor = valor;
    }

}
