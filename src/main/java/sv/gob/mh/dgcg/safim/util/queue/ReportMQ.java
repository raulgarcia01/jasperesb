/* Copyright 2013 - 2014
 * Dirección General de Contabilidad Gubernamental
 * Ministerio de Hacienda
 *
 * Este programa es de uso exclusivo para la Unidad de Informática
 * de la Dirección General de Contabilidad Gubernamental,
 * y está protegido por las leyes de derechos de autor de la
 * República de El Salvador, se prohibe la copia y distribución
 * total o parcial del mismo.
 */
package sv.gob.mh.dgcg.safim.util.queue;

import java.io.Serializable;
import java.util.List;

/**
 * Clase para los reportes en cola.
 *
 * @author Luis Felipe Sosa Alvarez <luisfsosa@gmail.com>
 */
public class ReportMQ implements Serializable {

    /**
     * Serial de la Clase.
     */
    private static final long serialVersionUID = -5358888938386497048L;

    /**
     * Nombre del Reporte.
     */
    private String reporte;

    /**
     * Formato del Reporte.
     */
    private String formato;

    /**
     * Nombre del Usuario.
     */
    private String usuario;

    /**
     * Correo del Usuario.
     */
    private String correo;

    /**
     * Parametros del Reporte.
     */
    private List<ParamMQ> parametros;

    /**
     * Retorna el valor del Nombre del Reporte.
     *
     * @return Nombre del Reporte
     */
    public String getReporte() {
        return reporte;
    }

    /**
     * Modifica el valor del Nombre del Reporte.
     *
     * @param Nombre
     *            del Reporte
     */
    public void setReporte(String reporte) {
        this.reporte = reporte;
    }

    /**
     * Retorna el valor del Formato del Reporte.
     *
     * @return Formato del Reporte
     */
    public String getFormato() {
        return formato;
    }

    /**
     * Modifica el valor Formato del Reporte.
     *
     * @param Formato
     *            del Reporte
     */
    public void setFormato(String formato) {
        this.formato = formato;
    }

    /**
     * Retorna el valor de Nombre de Usuario.
     *
     * @return Nombre de Usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Modifica el valor de Nombre de Usuario.
     *
     * @param Nombre
     *            de Usuario
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * Retorna el valor del Correo del Usuario.
     *
     * @return Correo del Usuario
     */
    public String getCorreo() {
        return correo;
    }

    /**
     * Modifica el valor del Correo del Usuario.
     *
     * @param Correo
     *            del Usuario
     */
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    /**
     * Retorna listado de Parametros.
     *
     * @return Listado de Parametros
     */
    public List<ParamMQ> getParametros() {
        return parametros;
    }

    /**
     * Modifica listado de parametros.
     *
     * @param Listado
     *            de Parametros.
     */
    public void setParametros(List<ParamMQ> parametros) {
        this.parametros = parametros;
    }

}
